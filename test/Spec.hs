import Test.QuickCheck
import Test.Hspec
import Data.Either (isLeft, isRight)
import ParserLib
import Parser
import Execution
import Shell
import System.IO hiding (stdin, stdout, stderr)
import System.Process (createPipe)
import Funsh (run)
import Control.Monad.IO.Class
import System.IO.Temp
import Data.String.Utils (startswith)

main :: IO ()
main = hspec $ do

  describe "Parser" $ do

    describe "Lib" $ do

      it "parses the empty string" $ parse "" pempty `shouldBe` Right ()
      it "parses characters" $ parse "a" (pchar (=='a')) `shouldBe` Right 'a'
      it "parses strings" $ parse "abc" (pstring "abc") `shouldBe` Right ()
      it "fails on missing content" $ parse "" (pstring "abc") `shouldSatisfy` isLeft
      it "parses first options" $ parse "abc" (por (pstring "abc") (pstring "def")) `shouldBe` Right ()
      it "parses second options" $ parse "def" (por (pstring "abc") (pstring "def")) `shouldBe` Right ()
      it "parses multiples" $ parse "aaa" (pplus (pstring "a") pempty) `shouldBe` Right [(), (), ()]
      it "parses multiples with separator" $ parse "ababa" (pplus (pstring "a") (pstring "b")) `shouldBe` Right [(), (), ()]
      it "does not parse empty multiples" $ parse "" (pplus (pstring "a") pempty) `shouldSatisfy` isLeft
      it "parses empty arbitraries" $ parse "" (pstar (pstring "a") pempty) `shouldBe` Right []
      it "does not parse wrong arbitraries" $ parse "b" (pstar (pstring "a") (pstring "b")) `shouldSatisfy` isLeft
      it "does not parse wrong arbitraries" $ parse "ba" (pstar (pstring "a") (pstring "b")) `shouldSatisfy` isLeft
      it "parses arbitraries" $ parse "ababa" (pstar (pstring "a") (pstring "b")) `shouldBe` Right [(), (), ()]
      it "obeyes positive lookahead" $ parse "abcdef" (do pnot (pstring "abc"); pstring "def") `shouldSatisfy` isLeft
      it "obeyes negative lookahead" $ parse "abcdef" (do pnot (pstring "def"); pstring "abcdef") `shouldSatisfy` isRight
      it "pany obeys the order of the parsers in the list" $ parse "abc" (pany [(do pstring "ab"; pstring "c" >> return "1"), (do pstring "a"; pstring "bc" >> return "2")]) `shouldBe` Right "1"

    describe "Bash" $ do

      describe "Expression" $ do

        it "parses variables" $ parse "abc" expression `shouldBe` Right (ExpressionVariable (Variable "abc"))
        it "parses numbers" $ parse "0" expression `shouldBe` Right (ExpressionNumber 0)
        it "parses numbers" $ parse "01" expression `shouldSatisfy` isLeft
        it "parses numbers" $ parse "6" expression `shouldBe` Right (ExpressionNumber 6)
        it "parses numbers" $ parse "643" expression `shouldBe` Right (ExpressionNumber 643)
        it "obeys unary operators" $ parse "+ 666" expression `shouldBe` Right (ExpressionUnary Add $ ExpressionNumber 666)
        it "obeys unary operators" $ parse "- 666" expression `shouldBe` Right (ExpressionUnary Subtract $ ExpressionNumber 666)
        it "parses multiplicative binary operators" $ parse "44 * -x / y" expression `shouldBe`
          Right (ExpressionBinary Divide 
            (ExpressionBinary Multiply
              (ExpressionNumber 44)
              (ExpressionUnary Subtract
                (ExpressionVariable (Variable "x"))))
            (ExpressionVariable $ Variable "y"))
        it "parses arithmetic with dot before dash" $ parse "3 + x * 4 - 5" expression `shouldBe`
          Right (ExpressionBinary Subtract
            (ExpressionBinary Add
              (ExpressionNumber 3)
                (ExpressionBinary Multiply
                  (ExpressionVariable $ Variable "x")
                  (ExpressionNumber 4)))
                (ExpressionNumber 5))
        it "parses inequality comparisons" $ parse "3+1 >= 5" expression `shouldBe` Right (ExpressionBinary GreaterEqual (ExpressionBinary Add (ExpressionNumber 3) (ExpressionNumber 1)) (ExpressionNumber 5))
        it "parses equality comparisons" $ parse "3 >= 5 == 2 < 3" expression `shouldBe` Right (ExpressionBinary Equal (ExpressionBinary GreaterEqual (ExpressionNumber 3) (ExpressionNumber 5)) (ExpressionBinary LessThan (ExpressionNumber 2) (ExpressionNumber 3)))
        it "does not parse multiple equality comparisons" $ parse "1 == 2 == 3" expression `shouldSatisfy` isLeft      
        it "parses conditionals" $ parse "1 == 2 && 5 || 6 ? 3 : 4" expression `shouldBe` Right 
          (ExpressionConditional
            (ExpressionBinary LOr
              (ExpressionBinary LAnd
                (ExpressionBinary Equal (ExpressionNumber 1) (ExpressionNumber 2))
                (ExpressionNumber 5))
              (ExpressionNumber 6))
            (ExpressionNumber 3)
            (ExpressionNumber 4))
        it "parses assignments" $ parse "x = y += 5" expression `shouldBe` Right (ExpressionAssignment (Variable "x") (ExpressionAssignment (Variable "y") (ExpressionBinary Add (ExpressionVariable $ Variable "y") (ExpressionNumber 5))))

      describe "Test" $ do
        it "parses equal matches" $ parse "abc== cde" test `shouldBe` Right (TestString Equal (Word [WordPartText "abc" WordPartContextNoQuote]) (Word [WordPartText "cde" WordPartContextNoQuote]))
        it "parses not equal matches" $ parse "abc !=cde" test `shouldBe` Right (TestString NotEqual (Word [WordPartText "abc" WordPartContextNoQuote]) (Word [WordPartText "cde" WordPartContextNoQuote]))
        it "parses less than matches" $ parse "abc< cde" test `shouldBe` Right (TestString LessThan (Word [WordPartText "abc" WordPartContextNoQuote]) (Word [WordPartText "cde" WordPartContextNoQuote]))
        it "parses greater than matches" $ parse "abc >cde" test `shouldBe` Right (TestString GreaterThan (Word [WordPartText "abc" WordPartContextNoQuote]) (Word [WordPartText "cde" WordPartContextNoQuote]))
        it "parses exists tests" $ parse "-e word" test `shouldBe` Right (TestExists $ Word [WordPartText "word" WordPartContextNoQuote])
        it "parses file exists tests" $ parse "-f word" test `shouldBe` Right (TestFileExists $ Word [WordPartText "word" WordPartContextNoQuote])
        it "parses directory exists tests" $ parse "-d word" test `shouldBe` Right (TestDirectoryExists $ Word [WordPartText "word" WordPartContextNoQuote])
        it "parses complex tests" $ parse "-f word1 || -d word2 && ! abc == def" test `shouldBe` Right
          (TestBinary LOr
            (TestFileExists $ Word [WordPartText "word1" WordPartContextNoQuote])
            (TestBinary LAnd
              (TestDirectoryExists $ Word [WordPartText "word2" WordPartContextNoQuote])
              (TestUnary LNegate
                (TestString Equal
                  (Word [WordPartText "abc" WordPartContextNoQuote])
                  (Word [WordPartText "def" WordPartContextNoQuote])))))

      describe "Word" $ do
        it "parses character quotes" $ parse "ab\\$cd" word `shouldBe` Right (Word [WordPartText "ab" WordPartContextNoQuote, WordPartText "$" WordPartContextSingleQuote, WordPartText "cd" WordPartContextNoQuote])
        it "parses character quotes" $ parse "ab\\=cd" word `shouldBe` Right (Word [WordPartText "ab" WordPartContextNoQuote, WordPartText "=" WordPartContextSingleQuote, WordPartText "cd" WordPartContextNoQuote])
        it "parses character quotes" $ parse "\\\\\\\"\\$" word `shouldBe` Right (Word [WordPartText "\\" WordPartContextSingleQuote, WordPartText "\"" WordPartContextSingleQuote, WordPartText "$" WordPartContextSingleQuote])
        it "parses substitutions" $ parse "\\$${xxx}" word `shouldBe` Right (Word [WordPartText "$" WordPartContextSingleQuote, WordPartVariableSubstitution (Variable "xxx") WordPartContextNoQuote])
        it "ignores singly quoted character quotes" $ parse "'\\$'" word `shouldBe` Right (Word [WordPartText "\\$" WordPartContextSingleQuote])
        it "parses stars" $ parse "ab*cd*" word `shouldBe` Right (Word [WordPartText "ab" WordPartContextNoQuote, WordPartStar WordPartContextNoQuote, WordPartText "cd" WordPartContextNoQuote, WordPartStar WordPartContextNoQuote])
        it "parses tildes" $ parse "~/ab~/cd~" word `shouldBe` Right (Word [WordPartTilde WordPartContextNoQuote, WordPartText "/ab~/cd~" WordPartContextNoQuote])
        it "parses the tilde word" $ parse "~" word `shouldBe` Right (Word [WordPartTilde WordPartContextNoQuote])
        it "does not parse the empty word" $ parse "" word `shouldSatisfy` isLeft

      describe "Assignment" $ do
        it "parses only valid assignments" $ parse "=zx" assignment `shouldSatisfy` isLeft
        it "parses empty assignments" $ parse "x=" assignment `shouldBe` Right (Assignment (Variable "x") (Word []))

  describe "Evaluation" $

    let 
        exec :: String -> IO String
        exec s = do
          (stdoutIn, stdinIn) <- createPipe
          (stdoutOut, stdinOut) <- createPipe
          withSystemTempDirectory "funsh-test-dir" $ \testPath -> do
            let config = Config { cfg_verbose = False }
            state <- makeInitialShellState testPath [("HOME", testPath), ("PATH", "/ffffsf/:/bin:/usr/bin/:/usr/local/bin:/run/current-system/sw/bin")]
            runShell True config state (run stdoutIn stdinOut True s)
            hGetContents stdoutOut
        runTest :: String -> String -> Expectation
        runTest command expectedOutput = do
          result <- liftIO $ exec command
          result `shouldBe` expectedOutput
        runTestFail :: String -> String -> Expectation
        runTestFail command expectedStart = do
          result <- liftIO $ exec command
          result `shouldSatisfy` startswith expectedStart
    in do

      describe "Arithmetic" $ do

        it "evaluates assignments" $ runTest "echo $((x = 2, y = 3, y -= 3 * x))" "-3\n"
        it "evaluates assignments" $ runTest "x=2; y=3;echo $(( y -= 3 * x))" "-3\n"
        it "evaluates conditionals" $ runTest "echo $((4 == 5 ? 43:44))" "44\n"
        it "evaluates conditionals" $ runTest "echo $((4 != 5 ? 43:44))" "43\n"
        it "evaluates arithmetic expressions" $ runTest "echo $(((-(-2+-3)**2 - 24) * 7 / 2 % 2))" "1\n"
        it "evaluates boolean expressions" $ runTest "echo $((1 && (77 || !0)))" "1\n"
        it "evaluates comparison expressions" $ runTest "echo $((5 != 4 && 5 == 6-1 && 3 <= 4 && 5 >= 3 && 3 < 4 && 4 > 3))" "1\n"
        it "evaluates comparison expressions" $ runTest "echo $((5 == 4 || 5 == 6-1 && 3 > 4 && 5 < 3 && 3 >= 4 && 4 <= 3))" "0\n"
        it "throws on divison by zero" $ runTest "echo $((5 / (4-4)))" "ExecutionError \"Division by zero\"\n"
        it "evaluates comparisions" $ runTest "echo $((4 == 5))" "0\n"
        it "evaluates comparisions" $ runTest "echo $((4 != 5))" "1\n"

      describe "Substitution" $ do

        it "substitutes variables" $ runTest "i=abc; echo \"0${i}1\"" "0abc1\n"
        it "substitutes arithmetic" $ runTest "i=6; echo \"0$((i*7))1\"" "0421\n"
        it "substitutes process codes" $ runTest "sh -c 'exit 43'; echo $?; echo $?; echo $?" "43\n0\n0\n"
        it "substitutes wildcards" $ runTest "touch a1b a2b a1c a2c z1c; ls -1 a*b; ls -1 a*" "a1b\na2b\na1b\na1c\na2b\na2c\n"
        it "recognizes missing home variable on tildes" $ runTestFail "HOME=; ls ~" "ExecutionError \"Missing"

      describe "Commands" $ do

        it "executes simple commands" $ runTest "echo abc" "abc\n"
        it "does not execute unknown commands" $ runTestFail "scheisshaus -v" "ExecutionError \"Unable to resolve path of scheisshaus\""
        it "evaluates for loops" $ runTest "for x in x0 x1; do for y in y0 y1; do echo ${x}${y}; done; done" "x0y0\nx0y1\nx1y0\nx1y1\n"
        it "evaluates empty for loops" $ runTest "for x in; do echo ${x}; done" ""
        it "evaluates empty for loops" $ runTest "xs=; for x in ${xs}; do echo ${x}; done" ""
        it "evaluates elements from variable" $ runTest "BLA='1 2 3'; for x in a${BLA}b; do echo ${x}; done" "a1\n2\n3b\n"
        it "evaluates C-for loops" $ runTest "for ((i=43; i<46; i+=1)); do echo $((i-40)); echo ${i}; done" "3\n43\n4\n44\n5\n45\n"
        it "evaluates C-for loops with variables" $ runTest "a=43; b=48; c=2; for ((i=a; i<=b; i+=c)); do echo ${i}; done" "43\n45\n47\n"
        it "evaluates empty C-for loops" $ runTest "for ((i=43; i < 33; i+=1)); do echo ${i}; done" ""
        it "evaluates until loops" $ runTest "((i=43)); until ((i>=46)); do echo $((i-40)); echo ${i}; ((i += 1)); done" "3\n43\n4\n44\n5\n45\n"
        it "evaluates while loops" $ runTest "((i=43)); while ((i<46)); do echo $((i-40)); echo ${i}; ((i += 1)); done" "3\n43\n4\n44\n5\n45\n"
        it "evaluates while loops with variables" $ runTest "a=4; b=7; while ((b > a)); do echo ${b}; ((b = b - 1)); done" "7\n6\n5\n"
        it "evaluates empty while loops" $ runTest "while [[ -e shit.txt ]] ; do echo xxx; done" ""
        it "evaluates if statements" $ runTest "((i=44)); if ((i > 40)); then echo JA; else echo NEIN; fi" "JA\n"
        it "evaluates if statements" $ runTest "((i=44)); if ((i < 40)); then echo JA; else echo NEIN; fi" "NEIN\n"

        describe "Conditional Statements" $ do
          it "evaluates conditional statements" $ runTest "if [[ -d xxx ]]; then echo JA; else echo NEIN; fi" "NEIN\n"
          it "evaluates conditional statements" $ runTest "echo bla > xxx; if [[ -d xxx ]]; then echo JA; else echo NEIN; fi" "NEIN\n"
          it "evaluates conditional statements" $ runTest "mkdir xxx; if [[ -d xxx ]]; then echo JA; else echo NEIN; fi" "JA\n"
          it "evaluates conditional statements" $ runTest "if [[ -f xxx ]]; then echo JA; else echo NEIN; fi" "NEIN\n"
          it "evaluates conditional statements" $ runTest "mkdir xxx; if [[ -f xxx ]]; then echo JA; else echo NEIN; fi" "NEIN\n"
          it "evaluates conditional statements" $ runTest "echo bla > xxx; if [[ -f xxx ]]; then echo JA; else echo NEIN; fi" "JA\n"
          it "evaluates conditional statements" $ runTest "if [[ -e xxx ]]; then echo JA; else echo NEIN; fi" "NEIN\n"
          it "evaluates conditional statements" $ runTest "mkdir xxx; if [[ -e xxx ]]; then echo JA; else echo NEIN; fi" "JA\n"
          it "evaluates conditional statements" $ runTest "echo bla > xxx; if [[ -e xxx ]]; then echo JA; else echo NEIN; fi" "JA\n"
          it "evaluates conditional statements" $ runTest "mkdir xxx; if [[ -d xxx && -f yyy ]]; then echo JA; else echo NEIN; fi" "NEIN\n"
          it "evaluates conditional statements" $ runTest "mkdir xxx; echo bla > yyy; if [[ -d xxx && -f yyy ]]; then echo JA; else echo NEIN; fi" "JA\n"
          it "evaluates conditional statements" $ runTest "echo bla > yyy ; if [[ -d xxx || -f yyy ]]; then echo JA; else echo NEIN; fi" "JA\n"
          it "evaluates conditional statements" $ runTest "[[ !bla==blb ]]; echo $?" "0\n"
          it "evaluates conditional statements" $ runTest "[[ !bla==bla ]]; echo $?" "1\n"
          it "evaluates conditional statements: le" $ runTest "x=bla; [[ ${x} <= blub ]]; echo $?" "0\n"
          it "evaluates conditional statements: lt" $ runTest "x=bla; [[ ${x} < blub ]]; echo $?" "0\n"
          it "evaluates conditional statements: ge" $ runTest "x=bla; [[ ${x} >= blub ]]; echo $?" "1\n"
          it "evaluates conditional statements: gt" $ runTest "x=bla; [[ ${x} > blub ]]; echo $?" "1\n"
          it "evaluates conditional statements: eq false" $ runTest "x=bla; [[ ${x} == blub ]]; echo $?" "1\n"
          it "evaluates conditional statements: ne true" $ runTest "x=bla; [[ ${x} != blub ]]; echo $?" "0\n"
          it "evaluates conditional statements: ne false" $ runTest "x=bla; [[ ${x} != bla ]]; echo $?" "1\n"
          it "evaluates conditional statements: eq true" $ runTest "x=bla; [[ ${x} == bla ]]; echo $?" "0\n"

        describe "Change Dir" $ do
          it "change directory works: not exists" $ runTest "cd xxx" "ExecutionError \"cd: directory does not exist\"\n"
          it "change directory works: is directory" $ runTest "touch xxx; chmod 777 xxx; cd xxx" "ExecutionError \"cd: directory does not exist\"\n"
          it "change directory works: no permission" $ runTest "mkdir --mode=000 xxx; cd xxx" "ExecutionError \"cd: user has no permission\"\n"
          it "change directory works: permission" $ runTest "mkdir --mode=500 xxx; cd xxx; ls -1a" ".\n..\n"
          it "change directory works" $ runTest "mkdir xxx; touch xxx/bla; cd xxx; ls -1a" ".\n..\nbla\n"

        describe "Environment" $ do
          it "environment variables are set properly" $ runTest "export BLA=blub; env | grep BLA; retain BLA; env | grep BLA; echo 1; export BLA; env | grep BLA" "BLA=blub\n1\nBLA=blub\n"
          it "environment variables are set properly" $ runTest "BLA=bla; env|grep BLA; echo 1; export BLA; env|grep BLA; BLA=blabla; env|grep BLA" "1\nBLA=bla\nBLA=blabla\n"
          it "obeys envvar assignments" $ runTest "BLA=bla env | grep BLA" "BLA=bla\n"
          it "allows empty variables" $ runTest "BLA= env | grep BLA" "BLA=\n"
          it "does not allow spaces" $ runTestFail "export BLA = bla; env | grep BLA" "Expression only"
          it "allows empty variables" $ runTest "export BLA= ; env | grep BLA" "BLA=\n"
          it "does not allow spaces" $ runTestFail "BLA = bla; echo ${BLA}" "ExecutionError"
          it "allows empty variables" $ runTest "BLA=; echo ${BLA}" "\n"

        describe "Redirection" $ do

          it "redirects" $ runTest "echo abc > blub.txt; cat < blub.txt; echo def >> blub.txt; cat < blub.txt" "abc\nabc\ndef\n"
          it "redirects" $ runTest "echo abczz > blub.txt; cat < blub.txt > bla.txt; cat bla.txt" "abczz\n"

      describe "Entry Point" $ do

        it "tolerates non-stripped input" $ runTest " echo abc" "abc\n"
        it "tolerates non-stripped input" $ runTest " echo abc " "abc\n"
        it "tolerates non-stripped input" $ runTest "echo abc " "abc\n"
        it "tolerates empty input" $ runTest "" ""

      describe "List" $ do

        it "evaluates lists" $ runTest "echo def && sh -c 'exit 44' && echo abc" "def\n"
        it "evaluates lists" $ runTest "sh -c 'exit 43' || echo abc || echo def" "abc\n"
        it "evaluates lists" $ runTest "sh -c 'exit 43' || echo abc && echo def" "abc\ndef\n"
        it "evaluates lists" $ runTest "sh -c 'exit 43' && echo abc || echo def" ""
        it "evaluates lists" $ runTest "sh -c 'exit 43' && echo abc || echo aaa; sh -c 'exit 43' || echo def && echo ddd; sh -c 'exit 43'; echo eee" "def\nddd\neee\n"

      describe "Pipeline" $ do

        it "evaluates pipelines" $ runTest "echo abcde | wc -m" "6\n"
        it "evaluates pipelines" $ runTest "echo abcde | tr [a-b] [A-B] | tr [c-d] [C-D]" "ABCDe\n"
