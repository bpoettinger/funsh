{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module PrinterGen where

import Control.Monad (mapM)
import Language.Haskell.TH
import Parser

data Node = Node String [Node]
          | Leaf String deriving (Show)

mkPrinters :: Q [Dec]
mkPrinters = do
    generated <- mapM (mkPrinter . mkName) ["Word", "Operator", "Expression", "Test", "Assignment", "Redirection", "List", "Command", "Pipeline"]
    handcrafted <- [d|
      render WordPartContextNoQuote t = t
      render WordPartContextSingleQuote t = Node "''" [t]
      render WordPartContextDoubleQuote t = Node "\"\"" [t] 
      printInt n = Leaf $ show n
      printVariable Variable { name=name } = Leaf name
      printWordPart WordPartText { wpt_text=t, wpt_context=c } = render c $ Leaf t
      printWordPart WordPartTilde { wpti_context=c } = render c $ Leaf "*"
      printWordPart WordPartStar { wpst_context=c} = render c $ Leaf "~"
      printWordPart WordPartVariableSubstitution { wpvs_var=Variable t, wpvs_context=c } = render c $ Leaf $ "${" ++ t ++ "}"
      printWordPart WordPartArithmeticSubstitution { wpas_expression=e, wpas_context=c } = render c $ Node "$((...))`" [printExpression e]
      printWordPart WordPartLastProcessCodeSubstitution { wplcs_context= c} = render c $ Leaf "$?"
      |]
    return $ generated ++ handcrafted

mkPrinter :: Name -> Q Dec
mkPrinter name = do
    info <- reify name
    let x = mkName "x"
    body <- case info of
        TyConI (DataD [] _ [] Nothing cons _) | all isTrivialCon cons -> mkPrinterBase x
        TyConI (DataD [] _ [] Nothing cons _) | all isRecordCon  cons -> mkPrinterRecursive x cons
        _ -> error "Unsupported type"
    return $ FunD (mkName $ "print" ++ (nameBase name)) [Clause [VarP x] (NormalB body) []]
  where
    isTrivialCon :: Con -> Bool
    isTrivialCon (NormalC _ []) = True
    isTrivialCon  _           = False
    isRecordCon :: Con -> Bool
    isRecordCon (RecC _ _) = True
    isRecordCon  _         = False


mkPrinterBase :: Name -> Q Exp
mkPrinterBase x = LamE [] <$> [| Leaf $ show $ $(return $ VarE x) |]

mkPrinterRecursive :: Name -> [Con] -> Q Exp
mkPrinterRecursive x cons = do
    nodeFn <- [| Node |]
    toList <- [| foldr (:) [] |]
    return $ CaseE (VarE x) (map (mkCase nodeFn toList) cons)
  where
    mkCase nodeFn toList (RecC name fields) = Match (ConP name $ pats) (NormalB $ AppE (AppE nodeFn name') (ListE fields')) []
      where
        name' = LitE $ StringL $ nameBase name
        bla (n, _, ConT t) = AppE (VarE $ mkName $ "print" ++ (nameBase t)) (AppE (VarE n) (VarE x))
        bla (n, _, AppT t1 (ConT t2)) = AppE (AppE nodeFn (LitE $ StringL $ nameBase t2 ++ "*")) $
            (AppE toList $ AppE
                (AppE (VarE $ mkName "fmap")
                      (LamE [VarP $ mkName "n"] (AppE (VarE $ mkName $ "print" ++ (nameBase t2)) (VarE $ mkName "n")))) (AppE (VarE n) (VarE x)))
        fields' = map bla fields
        pats = map (\(n,_,_) -> VarP $ mkName $ nameBase n) fields
