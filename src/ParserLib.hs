{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE RankNTypes #-}

module ParserLib (
    Parser,
    parse,
    pchar,
    pstring,
    pempty,
    pend,
    por,
    pany,
    pplus,
    pstar,
    pn,
    pquest,
    pnot,
    pnot',
  ) where

import Data.Maybe (maybe)
import Control.Monad (replicateM, when)

data ParserState = ParserState {
    text :: String
}

consumeCharacter :: ParserState -> (Maybe Char, ParserState)
consumeCharacter ParserState { text = text } = case text of
    []      -> (Nothing, ParserState { text = text  })
    c:text' -> (Just  c, ParserState { text = text' })

newtype Parser a = Parser {
  parse' :: ParserState -> (ParserState, Either String a)
}

parse :: String -> Parser a -> Either String a
parse text (Parser parse') =
    let state  = ParserState { text = text } in
    let result = parse' state in
    case result of
      (ParserState { text = text }, Left  s') -> Left $ "Error '" ++ s' ++ "' at '" ++ text ++ "'"
      (ParserState { text = []   }, Right s') -> Right s'
      (ParserState { text = text }, _       ) -> Left $ "Expression only reached until: '" ++ text ++ "'"

instance Functor Parser where
  f `fmap` p       = pure f <*> p

instance Applicative Parser where
  pure             = return 
  f <*> p          = f >>= (\f' -> p >>= \p' -> return $ f' p')

instance Monad Parser where
  return x              = Parser $ \s -> (s, Right x)
  (Parser parse') >>= f = Parser $ \s ->
    case parse' s of
      (s', Right a) -> let (Parser parse'') = f a in parse'' s'
      (s', Left  a) -> (s', Left a)

ptry :: Parser a -> (a -> b) -> (() -> b) -> Parser b
ptry (Parser parse') onSuccess onFail = Parser $ \s ->
  case parse' s of
    (_ , Left  _ ) -> (s , Right $ onFail    ())
    (s', Right a') -> (s', Right $ onSuccess a')

pchar :: (Char -> Bool) -> Parser Char
pchar test = Parser $ \s ->
  case consumeCharacter s of
    (Nothing, s')             -> (s', Left "Unexpectedly reached end of text")
    (Just  c, s') | test c    -> (s', Right c)
                  | otherwise -> (s', Left "Unexpected character in text")

pempty :: Parser ()
pempty = Parser $ \s -> (s, Right ())

pend :: Parser ()
pend = Parser $ \s -> case consumeCharacter s of
    (Nothing, _) -> (s, Right ())
    _            -> (s, Left  "Unexpected character in text")

pstring :: String -> Parser ()
pstring = mapM_ (pchar . (==))

por :: Parser a -> Parser a -> Parser a
por (Parser parse1') (Parser parse2') = Parser $ \s ->
    let (s', a) = parse1' s in
    case a of
      Left _ -> parse2' s
      _      -> (s', a)

pany :: [Parser a] -> Parser a
pany = foldl1 por

pnot :: Parser a -> Parser ()
pnot (Parser p) = Parser $ \s ->
    let (s', a) = p s in
    case a of
      Left _  -> (s, Right ())
      Right _ -> (s, Left "Unexpected match in text")

pnot' :: [Parser a] -> Parser ()
pnot' parsers = when ((not . null) parsers) ((pnot . pany) parsers)

pplus :: Parser a -> Parser b -> Parser [a]
pplus parser separator = do
    a <- parser
    as <- ptry (do separator; pplus parser separator) id (const [])
    return $ a : as

pstar :: Parser a -> Parser b -> Parser [a]
pstar parser separator = do
    a <- ptry parser Just (const Nothing)
    case a of
      Nothing -> return []
      Just a' -> do
        b <- aux
        return $ a' : b
  where
    aux = do
      x <- ptry (do separator; parser) Just (const Nothing)
      case x of
        Nothing -> return []
        Just x' -> do
          xs <- aux
          return $ x' : xs

pn :: Parser a -> Int -> Parser [a]
pn = flip replicateM 
    
pquest :: Parser a -> Parser (Maybe a)
pquest p = ptry p Just (const Nothing)

