module Shell where

import Parser
import Control.Monad.State.Strict
import Control.Monad.Loops
import Control.Monad.Reader
import Control.Monad.Except
import Control.Concurrent.STM
import Control.Concurrent
import qualified System.Environment
import System.Process
import Data.String.Utils (split)
import System.FilePath.Glob (globDir1, compile)
import System.Exit (ExitCode(..))
import qualified System.Directory
import System.FilePath (joinPath)
import Data.List ((\\), deleteBy, delete)
import Data.Maybe (fromMaybe)
import qualified System.Posix.Files
import qualified Control.Exception as Exception

data ShellException = DoesNotExistException | NoPermissionException deriving (Show)

data AsyncProcessState = Running | Done String deriving (Show, Eq)
newtype AsyncProcessId = AsyncProcessId Int deriving (Eq)
instance Show AsyncProcessId where
  show (AsyncProcessId id) = "[" ++ show id ++ "]"

data Process = ProcessFuture { pci_pid :: ProcessHandle }
             | ProcessDummy { pce_code :: ExitCode }

updateLastProcessCode :: ExitCode -> Shell ()
updateLastProcessCode code = modify $ \s -> s { es_lastProcessCode = code }

getProcessCode :: Process -> Shell ExitCode
getProcessCode ProcessFuture { pci_pid=pid } = do
    result <- liftIO $ Exception.try $ waitForProcess pid
    case (result :: Either Exception.AsyncException ExitCode) of
      Right code -> do
        updateLastProcessCode code
        return code
      Left Exception.ThreadKilled -> do
        liftIO $ terminateProcess pid
        return ExitSuccess
      Left err ->
        liftIO $ Exception.throw err
getProcessCode ProcessDummy { pce_code=code } = do
    updateLastProcessCode code
    return code

getProcessCode' :: Process -> Shell ExitCode
getProcessCode' ProcessFuture { pci_pid=pid } = liftIO $ waitForProcess pid
getProcessCode' ProcessDummy { pce_code=code } = return code

data AsyncProcessInfo = AsyncProcessInfo {
  api_id :: AsyncProcessId,
  api_thread :: ThreadId,
  api_state :: AsyncProcessState
}

data ShellState = ShellState {
  es_variables :: [(Variable, String)],
  es_exportedVariables :: [Variable],
  es_lastProcessCode :: ExitCode,
  es_currentDirectory :: FilePath,
  es_asyncProcesses :: TVar [AsyncProcessInfo]
}

data Config = Config {
  cfg_verbose :: Bool
}

type Shell = StateT ShellState (ReaderT Config IO)

setCurrentDirectory :: FilePath -> ExceptT ShellException Shell ()
setCurrentDirectory dir = do
    dir' <- lift $ resolvePathCanonical dir
    dir' <- checkDirectoryExists dir'
    dir' <- checkPathPermissions dir' True False True
    modify $ \s -> s { es_currentDirectory = dir' }

checkDirectoryExists :: FilePath -> ExceptT ShellException Shell FilePath
checkDirectoryExists path = do
    exists <- liftIO $ System.Directory.doesDirectoryExist path
    if not exists
      then throwError DoesNotExistException
      else return path

checkPathPermissions :: FilePath -> Bool -> Bool -> Bool -> ExceptT ShellException Shell FilePath
checkPathPermissions path read write execute = do
    allowed <- liftIO $ System.Posix.Files.fileAccess path read write execute
    if not allowed
      then throwError NoPermissionException
      else return path

getCurrentDirectory :: Shell FilePath
getCurrentDirectory = gets es_currentDirectory

resolvePathWith :: (FilePath -> IO FilePath) -> FilePath -> Shell FilePath
resolvePathWith f path = do
    cwd <- getCurrentDirectory
    let path' = case path of
                      "" -> ""
                      '/':_ -> path
                      _ -> joinPath [cwd, path]
    liftIO $ f path'

resolvePathAbsolute = resolvePathWith System.Directory.makeAbsolute
resolvePathCanonical = resolvePathWith System.Directory.canonicalizePath

withResolvedPath :: (FilePath -> IO a) -> FilePath -> Shell a
withResolvedPath f p = resolvePathCanonical p >>= \x -> liftIO $ f x

doesDirectoryExist :: FilePath -> Shell Bool
doesDirectoryExist = withResolvedPath System.Directory.doesDirectoryExist

doesFileExist :: FilePath -> Shell Bool
doesFileExist = withResolvedPath System.Directory.doesFileExist

doesPathExist :: FilePath -> Shell Bool
doesPathExist = withResolvedPath System.Directory.doesPathExist

resolveExecutablePath :: String -> ExceptT ShellException Shell FilePath
resolveExecutablePath executableName = do
    pathConcat <- lift $ getVariable $ Variable "PATH"
    let paths = split ":" pathConcat
    let pattern = compile executableName
    results <- liftIO $ mapM (globDir1 pattern) paths
    case concat results of
      [] -> throwError DoesNotExistException
      p:_ -> do
        return p

getEnvironment :: Shell [(Variable, String)]
getEnvironment = do
    exported <- gets es_exportedVariables
    filter (\(var,_) -> var `elem` exported) <$> gets es_variables

getVariable :: Variable -> Shell String
getVariable var = fromMaybe "" <$> gets (lookup var . es_variables)

setVariable :: Variable -> String -> Shell ()
setVariable var val = 
    modify $ \s -> s { es_variables = (var, val) : (delete' (var, undefined) $ es_variables s) }
  where
    delete' = deleteBy (\(x,_) (y,_) -> x == y)

exportVariable :: Variable -> Shell ()
exportVariable var = modify $ \s -> 
    let vars = es_exportedVariables s in
    if var `notElem` vars
      then s { es_exportedVariables = var : vars }
      else s

retainVariable :: Variable -> Shell ()
retainVariable var = modify $ \s -> s { es_exportedVariables = delete var (es_exportedVariables s) }

makeInitialShellState :: FilePath -> [(String, String)] -> IO ShellState
makeInitialShellState dir env = do
    asyncProcessesInitial <- atomically $ newTVar []
    let env' = map (\(var, val) -> (Variable var, val)) env
    return ShellState {
      es_variables = env',
      es_exportedVariables = map fst env',
      es_lastProcessCode = ExitSuccess,
      es_currentDirectory = dir,
      es_asyncProcesses = asyncProcessesInitial
    }

runShell :: Bool -> Config -> ShellState -> Shell a -> IO a
runShell isRootShell config state shell = do
    (a, _) <- runReaderT (runStateT (do a <- shell; when isRootShell killAllAsyncProcesses; return a) state) config
    return a
  where
    killAsyncProcess AsyncProcessInfo { api_thread=tid, api_state=Running } = killThread tid
    killAsyncProcess x = return ()

    killAllAsyncProcesses ::  Shell ()
    killAllAsyncProcesses = do
      asyncProcessesTVar <- gets es_asyncProcesses
      asyncProcesses <- liftIO $ atomically $ readTVar asyncProcessesTVar
      liftIO $ mapM_ killAsyncProcess asyncProcesses

