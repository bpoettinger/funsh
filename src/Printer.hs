{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}

module Printer where

import Prelude hiding (Word)
import Parser
import PrinterGen
import Language.Haskell.TH
import Diagrams.Prelude
import Diagrams.TwoD.Text
import Diagrams.TwoD.Size
import Diagrams.TwoD.Vector
import Diagrams.TwoD.Types
import Diagrams.Backend.SVG
import Diagrams.Combinators
import Control.Monad.State

mkPrinters

data PrinterState = PrinterState { counter :: Int }
type PrinterM = State PrinterState

makeId :: () -> PrinterM String
makeId _ = do
    s <- get
    let c = counter s
    put $ s { counter = c + 1 }
    return $ show c

printAst :: List -> IO ()
printAst list = 
    renderSVG "/tmp/debug.svg" (mkSizeSpec2D (Just (1000.0 :: Float)) (Just (1000.0 :: Float))) (d # bg white)
  where
    root = printList list
    (d, _) = runState (printNode root) (PrinterState 43)

myText t = alignedText 0.5 0.5 t # fontSizeL 0.2 <> rect 1.5 0.3 # lw 0

printNode (Leaf t) =
    return $ myText t

printNode (Node t children) = 
    if null children
      then return $ myText t
      else do
        children' <- mapM printNode children
        id0 <- makeId ()
        ids <- mapM (makeId . const ()) children'
        let children'' = map (\(c,i) -> c # named i) $ zip children' ids
        let total = myText t # named id0
        let result = vsep 0.5 [total, alignX 0 $ hsep 0.2 children'']
        let arrowOpts = with & headGap .~ 0.2 & tailGap .~ 0.5
        return $ foldl (\r id1 -> r # connectOutside' arrowOpts id0 id1) result ids
