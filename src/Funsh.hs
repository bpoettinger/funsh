module Funsh (run) where

import ParserLib
import Parser
import Execution
import Shell
import Control.Monad.State.Strict
import Control.Monad.Loops
import Control.Monad.Reader
import Control.Monad.Except
import System.IO hiding (stdin, stdout, stderr)
import Util
import Printer (printAst, printList)
import Data.String.Utils (strip)

run stdin stdout closeHandles command = do
    let command' = strip command
    unless (null command') (run' stdin stdout command')
    when closeHandles (liftIO $ hClose stdin >> hClose stdout)

run' :: Handle -> Handle -> String -> Shell ()
run' stdin stdout command =
    case parse command list of
      Left s ->
        liftIO $ hPutStrLnError stdout s
      Right c -> do
        whenM (reader cfg_verbose) $ liftIO $ printAst c

        code <- execute $ do process <- executeList stdin stdout c; lift $ getProcessCode process
        case code of
          Left err -> liftIO $ hPutStrLnError stdout $ show err
          _        -> return ()
