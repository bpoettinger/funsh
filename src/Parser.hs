module Parser where

import Prelude hiding (Word, words)
import ParserLib
import Control.Monad (when)
import Data.Char (ord, isDigit)
import Data.Maybe (isJust)

data Variable = Variable { name :: String } deriving (Show, Eq)
data WordPartContext = WordPartContextNoQuote | WordPartContextSingleQuote | WordPartContextDoubleQuote deriving (Show, Eq)
data WordPart = WordPartText { wpt_text :: String, wpt_context :: WordPartContext }
              | WordPartTilde { wpti_context :: WordPartContext }
              | WordPartStar { wpst_context :: WordPartContext }
              | WordPartVariableSubstitution { wpvs_var :: Variable, wpvs_context :: WordPartContext }
              | WordPartArithmeticSubstitution { wpas_expression :: Expression, wpas_context :: WordPartContext }
              | WordPartLastProcessCodeSubstitution { wplcs_context :: WordPartContext } deriving (Show, Eq)
data Word = Word { word_parts :: [WordPart] } deriving (Show, Eq)
data Operator = Add | Subtract | Multiply | Divide | Equal | NotEqual | LessThan | LessEqual | GreaterThan | GreaterEqual | Modulo | Exponentiate | LAnd | LOr | LNegate | Seq |
                Assign | AddAssign | SubtractAssign | MultiplyAssign | DivideAssign | ModuloAssign  deriving (Show, Eq)
data Expression = ExpressionBinary { eb_op :: Operator, eb_lhs :: Expression, eb_rhs :: Expression }
                | ExpressionUnary { eu_op :: Operator, eu_expr :: Expression }
                | ExpressionConditional { ec_cond :: Expression, ec_then :: Expression, ec_else :: Expression }
                | ExpressionVariable { ev_var :: Variable }
                | ExpressionAssignment { ea_var :: Variable, ea_rhs :: Expression }
                | ExpressionNumber { en_value :: Int } deriving (Show, Eq)
data Test = TestBinary { tb_op :: Operator, tb_lhs :: Test, tb_rhs :: Test }
          | TestUnary { tu_op :: Operator, tu_test :: Test }
          | TestString { ts_op :: Operator, ts_lhs :: Word, ts_rhs :: Word }
          | TestExists { te_path :: Word }
          | TestDirectoryExists { tde_path :: Word }
          | TestFileExists { tfe_path :: Word } deriving (Show, Eq)
data Assignment = Assignment { ass_variable :: Variable, ass_value :: Word } deriving (Show, Eq)
data Redirection = RedirectRead { redin_word :: Word }
                 | RedirectWrite { redout_word :: Word }
                 | RedirectAppend { redapp_word :: Word } deriving(Show)
data Command = CommandSimple { cmd_assignments :: [Assignment], cmd_words :: [Word], cmd_redirections :: [Redirection] }
             | CommandArithmetic { ca_expr :: Expression }
             | CommandConditional { cc_test :: Test }
             | CommandIf { cif_test :: List, cif_then :: List, cif_else :: Maybe List }
             | CommandFor { cfor_var :: Variable, cfor_values :: [Word], cfor_body :: List }
             | CommandForC { cforc_init :: Expression, cforc_cond :: Expression, cforc_inc :: Expression, cforc_body :: List }
             | CommandUntil { cuntil_test :: List, cuntil_body :: List }
             | CommandWhile { cwhile_test :: List, cwhile_body :: List }
             | CommandExport { ce_var :: Variable, ce_value :: Maybe Word }
             | CommandSet { cs_var :: Variable, cs_value :: Word }
             | CommandRetain { cu_var :: Variable }
             | CommandJobs { dummy :: Maybe Int }
             | CommandKill { ck_apid :: Int }
             | CommandCd { ccd_dir :: Word } deriving (Show)
data Pipeline = Pipeline { pipe_head :: Command, pipe_tail :: Pipeline }
              | SimplePipeline { pipe_command :: Command } deriving (Show)
data List = ListOne { lone_pipeline :: Pipeline }
          | ListAsync { lasync_list :: List }
          | ListSeq { lseq_first :: List, lseq_second :: List }
          | ListAnd { land_pipeline :: Pipeline, land_tail :: List }
          | ListOr { lor_pipeline :: Pipeline, lor_tail :: List } deriving (Show)

pnotText' :: [String] -> Parser ()
pnotText' = pnot' . (map pstring)

porTextToken' :: [(String, a)] -> Parser a
porTextToken' = pany . (map $ \(s,a) -> pstring s >> return a)

isFirstVariableChar c = let c' = ord c in c' >=(ord 'a') && c' <= (ord 'z') || c' >= (ord 'A') && c' <= (ord 'Z') || c == '_'
isLaterVariableChar c = isFirstVariableChar c || (let c' = ord c in c' >= ord '0' && c' <= ord '9')
isBlank c = c == ' ' || c == '\t'
isMetaChar c = c `elem` "()|&<>;"
isWordChar c = (not . isMetaChar) c && (not . isBlank) c

variable :: Parser Variable
variable = do
  x <- pchar isFirstVariableChar
  xs <- pstar (pchar isLaterVariableChar) pempty
  return $ Variable (x:xs)

blank0 :: Parser String
blank0 = pstar (pchar isBlank) pempty

blank1 :: Parser String
blank1 = pplus (pchar isBlank) pempty

wordPartVariableSubstitution :: WordPartContext -> Parser [WordPart]
wordPartVariableSubstitution ctx = do
  pstring "${"
  var <- variable
  pstring "}"
  return [WordPartVariableSubstitution { wpvs_var = var, wpvs_context = ctx }]

wordPartArithmeticSubstitution :: WordPartContext -> Parser [WordPart]
wordPartArithmeticSubstitution ctx = do
  pstring "$(("
  blank0
  expr <- expression
  blank0
  pstring "))"
  return [WordPartArithmeticSubstitution { wpas_expression = expr, wpas_context = ctx }]

wordPartLastProcessCodeSubstitution :: WordPartContext -> Parser [WordPart]
wordPartLastProcessCodeSubstitution ctx = do pstring "$?"; return [WordPartLastProcessCodeSubstitution ctx]

wordPartText :: [String] -> Parser [WordPart]
wordPartText forbiddenText = do
    s <- pplus (do pnotText' forbiddenFollowers; pchar isWordChar) pempty
    return [WordPartText s WordPartContextNoQuote]
  where
    forbiddenFollowers = ["${", "$((", "$?", "\\", "*", "\"", "'"] ++ forbiddenText

wordPartTextInDoubleContext :: Parser [WordPart]
wordPartTextInDoubleContext = do
    s <- pplus (do pnotText' forbiddenFollowers; pchar $ const True) pempty
    return [WordPartText s WordPartContextDoubleQuote]
  where
    forbiddenFollowers = ["${", "$((", "$?", "\\", "\""]

wordPartSingleQuote :: Parser [WordPart]
wordPartSingleQuote = do
  pstring "'"
  ss <- pplus (pchar (/='\'')) pempty
  pstring "'"
  return [WordPartText ss WordPartContextSingleQuote]

wordPartCharQuote :: Parser [WordPart]
wordPartCharQuote = do
  pstring "\\"
  c <- pchar (const True)
  return [WordPartText [c] WordPartContextSingleQuote]

wordPartCharQuoteInDoubleContext :: Parser [WordPart]
wordPartCharQuoteInDoubleContext = do
  pstring "\\"
  c <- (pstring "$" >> return "$") `por`
       (pstring "\"" >> return "\"") `por`
       (pstring "\\" >> return "\\") `por`
       (return "\\")
  return [WordPartText c WordPartContextDoubleQuote]

wordPartSingleChar :: Char -> WordPart -> Parser [WordPart]
wordPartSingleChar c p = pstring (c:[]) >> return [p]

wordPartDoubleQuote :: Parser [WordPart]
wordPartDoubleQuote = do
  pstring "\""
  ps <- pstar (pany [wordPartArithmeticSubstitution WordPartContextDoubleQuote,
                     wordPartVariableSubstitution WordPartContextDoubleQuote,
                     wordPartLastProcessCodeSubstitution WordPartContextDoubleQuote,
                     wordPartCharQuoteInDoubleContext,
                     wordPartTextInDoubleContext])
              pempty
  pstring "\""
  return $ concat ps


wordPart :: [String] -> Parser [WordPart]
wordPart forbiddenText = pany [wordPartArithmeticSubstitution WordPartContextNoQuote,
                               wordPartVariableSubstitution WordPartContextNoQuote,
                               wordPartLastProcessCodeSubstitution WordPartContextNoQuote,
                               wordPartSingleChar '*' (WordPartStar WordPartContextNoQuote),
                               wordPartCharQuote,
                               wordPartSingleQuote,
                               wordPartDoubleQuote,
                               wordPartText forbiddenText]

word' :: [String] -> Parser Word
word' forbiddenText = do
    tilde <- pquest $ wordPartSingleChar '~' (WordPartTilde WordPartContextNoQuote)
    let wordPart' = wordPart forbiddenText
    ss <- if isJust tilde then pstar wordPart' pempty else pplus wordPart' pempty
    return $ Word { word_parts=(maybe [] id tilde) ++ (concat ss) }

word :: Parser Word
word = word' []

number :: Parser Int
number = (do pchar (=='0'); pnot (pchar isDigit); return 0) `por` (number' 0 <$> (do pnot (pchar (=='0')); pplus (pchar isDigit) pempty))
  where
    number' a    []  = a
    number' a (d:ds) = number' (a * 10 + (ord d - ord '0')) ds

words :: Parser [Word]
words = pplus word blank1

unary constr inner ops = do
  op <- pquest $ porTextToken' ops
  case op of
    Nothing -> inner
    Just op' -> do blank0; constr op' <$> inner

binary constr inner ops = do
    lhs <- inner
    (do
      blank0
      op <- porTextToken' ops
      blank0
      constr op lhs <$> inner) `por`
      (return lhs)

binaryr constr inner ops = do
    lhs <- inner
    binary' lhs
  where
    binary' lhs =
      (do
        blank0
        op <- porTextToken' ops
        blank0
        rhs <- inner
        binary' $ constr op lhs rhs) `por`
      (return lhs)

test :: Parser Test
test = t3
  where
    check s c = pstring s >> blank1 >> c <$> word
    stringOperators = [("==", Equal), ("!=", NotEqual), ("<=", LessEqual), (">=", GreaterEqual), (">", GreaterThan), ("<", LessThan)]
    t0 = (check "-f" TestFileExists) `por`
         (check "-d" TestDirectoryExists) `por`
         (check "-e" TestExists) `por`
         (do pstring "("; blank0; e <- t3; blank0; pstring ")"; return e) `por`
         (do
           lhs <- word' $ map fst stringOperators
           blank0
           op <- porTextToken' stringOperators
           blank0
           rhs <- word' $ map fst stringOperators
           return $ TestString op lhs rhs)
    t1 = unary  TestUnary  t0 [("!", LNegate)]
    t2 = binary TestBinary t1 [("&&", LAnd)]
    t3 = binary TestBinary t2 [("||", LOr)]

expression :: Parser Expression
expression = eb
  where
    assign inner ops =
        (do
          var <- variable
          blank0
          op <- porTextToken' ops
          blank0
          pnot $ pstring "="
          rhs <- assign inner ops `por` inner
          let rhs' = case op of
                            Assign -> rhs
                            _      -> ExpressionBinary (convertOp op) (ExpressionVariable var) rhs
          return $ ExpressionAssignment { ea_var = var, ea_rhs = rhs' }) `por`
         inner
      where
        convertOp AddAssign = Add
        convertOp SubtractAssign = Subtract
        convertOp MultiplyAssign = Multiply
        convertOp DivideAssign = Divide
        convertOp ModuloAssign = Modulo

    e0 = (do pstring "("; blank0; e <- eb; blank0; pstring ")"; return e) `por`
         (ExpressionVariable <$> variable) `por`
         (ExpressionNumber <$> number)
    e1 = unary   ExpressionUnary  e0 [("+", Add), ("-", Subtract), ("!", LNegate)]
    e2 = binaryr ExpressionBinary e1 [("**", Exponentiate)]
    e3 = binaryr ExpressionBinary e2 [("*", Multiply), ("/", Divide), ("%", Modulo)]
    e4 = binaryr ExpressionBinary e3 [("+", Add), ("-", Subtract)]
    e5 = binary  ExpressionBinary e4 [("<=", LessEqual), (">=", GreaterEqual), ("<", LessThan), (">", GreaterThan)]
    e6 = binary  ExpressionBinary e5 [("==", Equal), ("!=", NotEqual)]
    e7 = binaryr ExpressionBinary e6 [("&&", LAnd)]
    e8 = binaryr ExpressionBinary e7 [("||", LOr)]
    e9 = do
      cond <- e8
      (do blank0; pstring "?"; blank0; ea <- e8; blank0; pstring ":"; blank0; ExpressionConditional cond ea <$> e8) `por` return cond
    ea = assign                   e9 [("=", Assign), ("+=", AddAssign), ("-=", SubtractAssign), ("*=", MultiplyAssign), ("/=", DivideAssign), ("%=", ModuloAssign)]
    eb = binaryr ExpressionBinary ea [(",", Seq)]


assignment :: Parser Assignment
assignment = (
  do
   k <- variable
   pstring "="
   v <- word
   return $ Assignment { ass_variable=k, ass_value=v }) `por` (do k <- variable; pstring "="; return $ Assignment k (Word []))

assignments :: Parser [Assignment]
assignments = pstar assignment blank1

redirection :: Parser Redirection
redirection = (do n <- pquest number; pstring ">"; blank0; RedirectWrite <$> word ) `por`
              (do n <- pquest number; pstring "<"; blank0; RedirectRead <$> word ) `por`
              (do n <- pquest number; pstring ">>"; blank0; RedirectAppend <$> word )

redirections :: Parser [Redirection]
redirections = pstar redirection blank1

commandSimple :: Parser Command
commandSimple = do
  as <- assignments
  case as of
    [] -> return ""
    _  -> blank1
  ws <- pplus (do pnot (number >> pstring ">"); word) blank1
  blank0
  rs <- redirections
  return $ CommandSimple { cmd_assignments=as, cmd_words=ws, cmd_redirections=rs }

commandIf :: Parser Command
commandIf = do
  pstring "if"
  blank1
  listTest <- list' ["then"]
  blank0
  pstring ";"
  blank0
  pstring "then"
  blank1
  listThen <- list' ["else", "fi"]
  blank0
  pstring ";"
  blank0
  listElse <- pquest (do pstring "else"; blank1; listElse' <- list' ["fi"]; blank0; pstring ";"; blank0; return listElse')
  pstring "fi"
  return $ CommandIf { cif_test=listTest, cif_then=listThen, cif_else=listElse }

commandFor :: Parser Command
commandFor = do
  pstring "for"
  blank1
  v <- variable
  blank1
  pstring "in"
  ws <- (do blank1; pstar word blank1) `por` (return [])
  blank0
  pstring ";"
  blank0
  pstring "do"
  blank1
  body <- list' ["done"]
  blank0
  pstring ";"
  blank0
  pstring "done"
  return $ CommandFor { cfor_var=v, cfor_values=ws, cfor_body=body }

commandForC :: Parser Command
commandForC = do
  pstring "for"
  blank0
  pstring "(("
  blank0
  exprInit <- expression
  blank0
  pstring ";"
  blank0
  exprCond <- expression
  blank0
  pstring ";"
  blank0
  exprInc <- expression
  blank0
  pstring "))"
  blank0
  pstring ";"
  blank0
  pstring "do"
  blank1
  exprBody <- list' ["done"]
  blank0
  pstring ";"
  blank0
  pstring "done"
  return $ CommandForC { cforc_init=exprInit, cforc_cond=exprCond, cforc_inc=exprInc, cforc_body=exprBody }

commandUntil :: Parser Command
commandUntil = do
  pstring "until"
  blank1
  exprTest <- list' ["do"]
  blank0
  pstring ";"
  blank0
  pstring "do"
  blank1
  exprBody <- list' ["done"]
  blank0
  pstring ";"
  blank0
  pstring "done"
  return $ CommandUntil { cuntil_test = exprTest, cuntil_body = exprBody }

commandWhile :: Parser Command
commandWhile = do
  pstring "while"
  blank1
  exprTest <- list' ["do"]
  blank0
  pstring ";"
  blank0
  pstring "do"
  blank1
  exprBody <- list' ["done"]
  blank0
  pstring ";"
  blank0
  pstring "done"
  return $ CommandWhile { cwhile_test=exprTest, cwhile_body=exprBody }

commandArithmetic :: Parser Command
commandArithmetic = do
   pstring "(("
   blank0
   e <- expression
   blank0
   pstring "))"
   return $ CommandArithmetic e

commandConditional :: Parser Command
commandConditional = do
  pstring "[["
  blank1
  t <- test
  blank1
  pstring "]]"
  return $ CommandConditional t

commandCd :: Parser Command
commandCd = do
  pstring "cd"
  blank1
  destDir <- word
  return $ CommandCd destDir

commandExport :: Parser Command
commandExport = do
  pstring "export"
  blank1
  var <- variable
  (do pstring "="
      w <- word
      return $ CommandExport var $ Just w) `por` (do pstring "="; return $ CommandExport var $ Just $ Word []) `por` (return $ CommandExport var Nothing)

commandSet :: Parser Command
commandSet = (do
  var <- variable
  pstring "="
  w <- word
  return $ CommandSet var w) `por` (do v <- variable; pstring "="; return $ CommandSet v $ Word [])

commandRetain :: Parser Command
commandRetain = do
  pstring "retain"
  blank1
  CommandRetain <$> variable

commandJobs :: Parser Command
commandJobs = do
  pstring "jobs"
  return $ CommandJobs Nothing

commandKill :: Parser Command
commandKill = do
  pstring "kill"
  blank1
  CommandKill <$> number

command :: Parser Command
command = commandCd `por`
          commandJobs `por`
          commandKill `por`
          commandExport `por`
          commandRetain `por`
          commandIf `por`
          commandFor `por`
          commandForC `por`
          commandUntil `por`
          commandWhile `por`
          commandArithmetic `por`
          commandConditional `por`
          commandSimple `por`
          commandSet


pipeline :: Parser Pipeline
pipeline = do
  c <- command
  por (do
      blank0
      pstring "|"
      blank0
      p <- pipeline
      return $ Pipeline { pipe_head=c, pipe_tail=p })
    (return $ SimplePipeline { pipe_command=c })

list1 :: Parser List
list1 = do
  p <- pipeline
  (do
    blank0
    pstring "&&"
    blank0
    l <- list1
    return $ ListAnd { land_pipeline=p, land_tail=l }
   ) `por` (do
     blank0
     pstring "||"
     blank0
     l <- list1
     return $ ListOr { lor_pipeline=p, lor_tail=l }
    ) `por` (
      return $ ListOne p
     )

list' :: [String] -> Parser List
list' forbiddenFollowers = do
    p <- list1
    m <- pquest (do blank0; pstring "&")
    let p' = maybe p (const $ ListAsync p) m
    (do
      blank0
      pstring ";"
      blank0
      pnotText' forbiddenFollowers
      l <- list' forbiddenFollowers
      return $ ListSeq { lseq_first = p', lseq_second = l }) `por` (return p')

list :: Parser List
list = list' []
