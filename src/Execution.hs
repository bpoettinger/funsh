{-# LANGUAGE TupleSections #-}

module Execution where

import Prelude hiding (Word)
import Data.Maybe (isJust, fromJust)
import Data.List (deleteBy, delete, (\\), stripPrefix)
import Data.String.Utils (replace, splitWs)
import Control.Monad.State.Strict
import Control.Monad.Loops
import Control.Monad.Reader
import Control.Monad.Except
import qualified Control.Exception as Exception
import System.Process
import System.FilePath (makeRelative, joinPath)
import System.FilePath.Glob (compileWith, CompOptions(..), globDir)
import System.IO.Error (isDoesNotExistError, isPermissionError)
import System.Exit (ExitCode(..))
import System.IO hiding (stdin, stdout, stderr)
import qualified System.IO (stdin, stdout)
import Util
import Parser
import Control.Concurrent.STM
import Control.Concurrent
import Shell

data ExecutionError = ExecutionError String deriving (Eq, Show)

type Execution = ExceptT ExecutionError Shell

execute :: Execution a -> Shell (Either ExecutionError a)
execute = runExceptT

importAssignments = map (\(k,v) -> Assignment (Variable k) (Word $ [WordPartText v WordPartContextNoQuote]))

forkProcess :: [(Variable, String)] -> FilePath -> String -> [String] -> Handle -> Handle -> IO ProcessHandle
forkProcess env dir program vars stdin stdout = do
    let config = CreateProcess {
      cmdspec = RawCommand program vars,
      cwd = Just dir,
      env = Just $ map (\(x,y) -> (name x, y)) env,
      std_in = UseHandle $ stdin,
      std_out = UseHandle $ stdout,
      std_err = UseHandle $ stdout,
      close_fds = True,
      create_group = False,
      delegate_ctlc = False,
      detach_console = False,
      create_new_console = False,
      new_session = False,
      child_group = Nothing,
      child_user = Nothing,
      use_process_jobs = False
    }
    (_, _, _, pid) <- createProcess_ "" config
    return pid

data CommandConfig = CommandConfig {
  cc_stdingen :: Execution Handle,
  cc_stdinkill :: Handle -> Execution (),
  cc_stdoutgen :: Execution Handle,
  cc_stdoutkill :: Handle -> Execution () 
}

openFile' :: FilePath -> IOMode -> Execution Handle
openFile' path mode = do
    -- openFile: IOError on
    --  * AlreadyInUseError:
    --  * DoesNotExistError:
    --  * PermissionError:
    -- All exceptions are not errors, pass to user.
    path' <- lift $ resolvePathCanonical path
    handle <- liftIO $ Exception.try $ openFile path' mode
    case (handle :: Either Exception.IOException Handle) of
      Left err -> throwError $ ExecutionError $ "Failed opening file '" ++ path ++ "': " ++ show err
      Right handle -> return handle

applyRedirection :: CommandConfig -> Redirection -> Execution CommandConfig
applyRedirection config RedirectRead { redin_word=path } = do
  path' <- resolveWordUniquely path
  let stdin = openFile' path' ReadMode
  return $ config { cc_stdingen = stdin, cc_stdinkill = \h -> liftIO (hClose h) }
applyRedirection config RedirectWrite { redout_word=path } = do
  path' <- resolveWordUniquely path
  let stdout = openFile' path' WriteMode
  return $ config { cc_stdoutgen = stdout, cc_stdoutkill = \h -> liftIO (hClose h) }
applyRedirection config RedirectAppend { redapp_word=path } = do
  path' <- resolveWordUniquely path
  let stdout = openFile' path' AppendMode
  return $ config { cc_stdoutgen = stdout, cc_stdoutkill = \h -> liftIO (hClose h) }

applyRedirections :: CommandConfig -> [Redirection] -> Execution CommandConfig
applyRedirections = foldM applyRedirection

evaluateExpression :: Expression -> Execution Int
evaluateExpression ExpressionNumber { en_value = i} = return i
evaluateExpression ExpressionVariable { ev_var = var } = do val <- lift $ getVariable var; return $ read val
evaluateExpression ExpressionConditional { ec_cond=econd, ec_then=ethen, ec_else=eelse } = do
  val <- evaluateExpression econd
  if val /= 0
    then evaluateExpression ethen
    else evaluateExpression eelse
evaluateExpression ExpressionUnary { eu_op = op, eu_expr = expr } = do
  val <- evaluateExpression expr
  case op of
    Add -> return val
    Subtract -> return $ (-val)
    LNegate -> return $ if val == 0 then 1 else 0
evaluateExpression ExpressionBinary { eb_lhs=lhs, eb_rhs=rhs, eb_op=op } = do
  lhs' <- evaluateExpression lhs
  rhs' <- evaluateExpression rhs
  case op of
    Add -> return $ lhs' + rhs'
    Subtract -> return $ lhs' - rhs'
    Multiply -> return $ lhs' * rhs'
    Divide -> if rhs' /= 0 then return (lhs' `div` rhs') else throwError (ExecutionError "Division by zero")
    Modulo -> return $ lhs' `mod` rhs'
    Exponentiate -> return $ lhs'^rhs'
    Seq -> return $ rhs'
    Equal -> return $ if lhs' == rhs' then 1 else 0
    NotEqual -> return $ if lhs' /= rhs' then 1 else 0
    LessThan -> return $ if lhs' < rhs' then 1 else 0
    LessEqual -> return $ if lhs' <= rhs' then 1 else 0
    GreaterThan -> return $ if lhs' > rhs' then 1 else 0
    GreaterEqual -> return $ if lhs' >= rhs' then 1 else 0
    LAnd -> return $ if lhs' == 1 && rhs' == 1 then 1 else 0
    LOr -> return $ if lhs' == 1 || rhs' == 1 then 1 else 0
evaluateExpression ExpressionAssignment { ea_var=var, ea_rhs=rhs } = do
  val <- evaluateExpression rhs
  lift $ setVariable var $ show val
  return val

evaluateTest :: Test -> Execution Bool
evaluateTest TestUnary { tu_op=op, tu_test=test} = not <$> evaluateTest test
evaluateTest TestBinary { tb_op=op, tb_lhs=lhs, tb_rhs=rhs } = do
  lhs' <- evaluateTest lhs
  case op of
    LAnd -> if lhs' then evaluateTest rhs else return False
    LOr  -> if lhs' then return True else evaluateTest rhs
evaluateTest TestString { ts_op=op, ts_lhs=lhs, ts_rhs=rhs } = do
  lhs' <- resolveWordUniquely lhs
  rhs' <- resolveWordUniquely rhs
  case op of
    Equal -> return $ lhs' == rhs'
    NotEqual -> return $ lhs' /= rhs'
    LessThan -> return $ lhs' < rhs'
    GreaterThan -> return $ lhs' > rhs'
    LessEqual -> return $ lhs' <= rhs'
    GreaterEqual -> return $ lhs' >= rhs'
evaluateTest (TestDirectoryExists path) = do
  path' <- resolveWordUniquely path
  -- doesDirectoryExist: no exceptions
  lift $ doesDirectoryExist path'
evaluateTest (TestFileExists path) = do
  path' <- resolveWordUniquely path
  -- doesFileExist: no exceptions
  lift $ doesFileExist path'
evaluateTest (TestExists path) = do
  path' <- resolveWordUniquely path
  -- doesPathExist: no exceptions
  lift $ doesPathExist path'

resolveWordUniquely :: Word -> Execution String
resolveWordUniquely w = do
  words <- resolveWord w
  if length words > 1
    then throwError $ ExecutionError "Word expands non-uniquely in place where expected"
    else return $ head words

resolveWord :: Word -> Execution [String]
resolveWord Word { word_parts=parts } = do
    let opts = CompOptions {
      characterClasses = False,
      characterRanges = True,
      numberRanges = False,
      wildcards = True,
      recursiveWildcards = False,
      pathSepInRanges = False,
      errorRecovery = False
    }
    parts' <- mapM aux parts
    let parts'' = split parts'
    let globStrings = filter (not . null) $ map concat parts''
    let globPatterns = map (compileWith opts) globStrings
    dir <- lift $ getCurrentDirectory
    -- globDir: exceptions are errors
    matches <- liftIO $ globDir globPatterns dir
    let resultAbs = concat $ map (uncurry selectMatchesOrFallBack) $ zip matches globStrings
    -- makeRelative: exceptions are errors
    return $ map (makeRelative dir) resultAbs
  where
    selectMatchesOrFallBack [] original = [replace "[*]" "*" $ replace "[[]" "[" $ replace "[]]" "]" $ original]
    selectMatchesOrFallBack ms _        = ms

    replaceStars = (replace "*" "[*]") . (replace "[" "[[]") . (replace "]" "[]]")

    aux WordPartText { wpt_text = text, wpt_context = context } = return $ (replaceStars text, context)
    aux WordPartVariableSubstitution { wpvs_var=var, wpvs_context = context } = (,context) <$> replaceStars <$> (lift $ getVariable var)
    aux WordPartArithmeticSubstitution { wpas_expression=expr, wpas_context = context } = (,context) <$> replaceStars <$> show <$> evaluateExpression expr
    aux WordPartLastProcessCodeSubstitution { wplcs_context = context } = do
      s <- get
      return $ case es_lastProcessCode s of
        ExitSuccess -> ("0", context)
        ExitFailure n -> (show n, context)
    aux WordPartStar { wpst_context = context } = return ("*", context)
    aux WordPartTilde { wpti_context = context } = (,context) <$> extractHome
      where
        extractHome :: Execution String
        extractHome = do
          result <- lift $ getVariable $ Variable "HOME"
          case result of
            "" -> throwError $ ExecutionError "Missing environment variable HOME when resolving ~, please specify"
            _ -> return result

    decomposeList (x0:xs') = (x0, xs, x1)
      where
        decomposeList' acc (x1:[]) = (reverse acc, x1)
        decomposeList' acc (x :xs) = decomposeList' (x : acc) xs
        (xs, x1) = decomposeList' [] xs'

    splitString (s, WordPartContextNoQuote    ) = splitWs s
    splitString (s, WordPartContextSingleQuote) = [s]
    splitString (s, WordPartContextDoubleQuote) = [s]

    split' wAcc pAcc    []  = reverse $ map reverse (pAcc : wAcc)
    split' wAcc pAcc (w:ws) = split' wAcc' pAcc' ws
      where
        w' = reverse $ splitString w
        wAcc' = if null w' then wAcc else if length w' >= 2 then let (x0, xs, x1) = decomposeList w' in if null xs then (x1 : pAcc) : wAcc else (map (:[]) xs) ++ ((x1 : pAcc) : wAcc) else wAcc
        pAcc' = if null w' then pAcc else if length w' >= 2 then (head w') : [] else (head w') : pAcc

    split = split' [] []

resolveWords :: [Word] -> Execution [String]
resolveWords words = concat <$> mapM resolveWord words

executeCommand :: Handle -> Handle -> Command -> Execution Process
executeCommand stdin stdout CommandSimple { cmd_assignments=assignments, cmd_words=words, cmd_redirections=redirections } = do
    env <- lift $ getEnvironment
    let childEnv = mergeEnvs env assignments
    (command:args) <- resolveWords words

    command <- if '/' `elem` command
      then lift $ resolvePathAbsolute command
      else do
        result <- lift $ runExceptT $ resolveExecutablePath command
        case result of
          Left DoesNotExistException -> throwError $ ExecutionError $ "Unable to resolve path of " ++ command
          Right p -> return p

    let initialCommandConfig = CommandConfig {
      cc_stdingen = return stdin,
      cc_stdinkill = constM (),
      cc_stdoutgen = return stdout,
      cc_stdoutkill = constM ()
    }
    commandConfig <- applyRedirections initialCommandConfig redirections
    dir <- lift $ getCurrentDirectory
    stdin' <- cc_stdingen commandConfig
    myFinally (do
      stdout' <- cc_stdoutgen commandConfig

      -- forkProcess/createProcess_: exception is error except for unknown commands
      myFinally (do
        pid <- liftIO $ Exception.try $ forkProcess childEnv dir command args stdin' stdout'
        case (pid :: Either Exception.IOException ProcessHandle) of
          Left err | isDoesNotExistError err -> throwError $ ExecutionError $ "Unknown executable " ++ command
          Left err -> liftIO $ Exception.throw err
          Right pid -> return $ ProcessFuture pid)
        ((cc_stdoutkill commandConfig) stdout'))
      ((cc_stdinkill commandConfig) stdin')
  where
    mergeEnvs :: [(Variable, String)] -> [Assignment] -> [(Variable, String)]
    mergeEnvs as bs = as' ++ bs'
      where
        bs' = map (\a -> (ass_variable a, foldl (\a p -> a ++ (wpt_text p)) "" $ word_parts $ ass_value a)) bs
        bks = map fst bs'
        as' = filter ((`notElem` bks) . fst) as

executeCommand stdin stdout CommandIf { cif_test=ltest, cif_then=lthen, cif_else=lelse } = do
  process <- executeList stdin stdout ltest
  code <- lift $ getProcessCode process
  case code of 
    ExitSuccess -> executeList stdin stdout lthen
    _ -> case lelse of
      Just l -> executeList stdin stdout l
      _ -> return $ ProcessDummy ExitSuccess

executeCommand stdin stdout CommandFor { cfor_var=var, cfor_body=body, cfor_values=values } = do
    values' <- resolveWords values
    foldM step (ProcessDummy ExitSuccess) values'
  where
    step :: Process -> String -> Execution Process
    step prevProcess value = do
      lift $ getProcessCode prevProcess
      lift $ setVariable var value
      executeList stdin stdout body

executeCommand stdin stdout CommandForC { cforc_init=init, cforc_cond=cond, cforc_inc=inc, cforc_body=body } = do
    evaluateExpression init
    ProcessDummy <$> loop ExitSuccess
  where
    loop :: ExitCode -> Execution ExitCode
    loop exit = do
      b <- evaluateExpression cond
      if b == 1
        then do
          pid <- executeList stdin stdout body
          exit' <- lift $ getProcessCode pid
          evaluateExpression inc
          loop exit'
        else
          return exit

executeCommand stdin stdout CommandUntil { cuntil_test=test, cuntil_body = body } = do
    ProcessDummy <$> loop ExitSuccess
  where
    loop exit = do
      pid <- executeList stdin stdout test
      b <- lift $ getProcessCode pid
      if b /= ExitSuccess
        then do
          pid <- executeList stdin stdout body
          exit' <- lift $ getProcessCode pid
          loop exit'
        else
          return exit

executeCommand stdin stdout CommandWhile { cwhile_test=test, cwhile_body=body } = do
    ProcessDummy <$> loop ExitSuccess
  where
    loop exit = do
      pid <- executeList stdin stdout test
      b <- lift $ getProcessCode pid
      if b == ExitSuccess
        then do
          pid <- executeList stdin stdout body
          exit' <- lift $ getProcessCode pid
          loop exit'
        else
          return exit
  
executeCommand stdin stdout CommandArithmetic { ca_expr=expr } = do
    value <- evaluateExpression expr
    return $ ProcessDummy $ if value == 0 then ExitFailure 1 else ExitSuccess

executeCommand stdin stdout CommandConditional { cc_test=test } = do
    value <- evaluateTest test
    return $ ProcessDummy $ if value then ExitSuccess else ExitFailure 1

executeCommand stdin stdout CommandCd { ccd_dir=dir } = do
    dir' <- resolveWordUniquely dir
    result <- lift $ runExceptT $ setCurrentDirectory dir'
    case result of
      Right _                    -> return     $ ProcessDummy ExitSuccess
      Left DoesNotExistException -> throwError $ ExecutionError "cd: directory does not exist"
      Left NoPermissionException -> throwError $ ExecutionError "cd: user has no permission"

executeCommand stdin stdout CommandExport { ce_var=var, ce_value=val } = do
    case val of
      Nothing -> return ()
      Just (Word []) ->
        lift $ setVariable var ""
      Just val' -> do
        val'' <- resolveWordUniquely val'
        lift $ setVariable var val''
    lift $ exportVariable var
    return $ ProcessDummy ExitSuccess

executeCommand stdin stdout CommandSet { cs_var=var, cs_value=Word [] } = do
    lift $ setVariable var ""
    return $ ProcessDummy ExitSuccess

executeCommand stdin stdout CommandSet { cs_var=var, cs_value=val } = do
    val' <- resolveWordUniquely val
    lift $ setVariable var val'
    return $ ProcessDummy ExitSuccess

executeCommand stdin stdout CommandRetain { cu_var=var } = do
    lift $ retainVariable var
    return $ ProcessDummy ExitSuccess

executeCommand stdin stdout CommandJobs {} = do
    asyncProcessesTVar <- lift $ gets es_asyncProcesses
    asyncProcesses <- liftIO $ atomically $ readTVar asyncProcessesTVar
    liftIO $ mapM (putStrLn . renderAsyncProcessInfo) asyncProcesses
    return $ ProcessDummy ExitSuccess
  where
    renderAsyncProcessInfo AsyncProcessInfo { api_id=id, api_state=state } = show id ++ " " ++ show state

executeCommand stdin stdout CommandKill { ck_apid=apid } = do
    asyncProcessesTVar <- gets es_asyncProcesses
    asyncProcesses <- liftIO $ atomically $ readTVar asyncProcessesTVar
    liftIO $ mapM_ killAsyncProcess asyncProcesses
    return $ ProcessDummy ExitSuccess
  where
    -- terminateProcess: exception is error.
    killAsyncProcess AsyncProcessInfo { api_id=AsyncProcessId apid', api_thread=tid, api_state=Running } | apid == apid' = killThread tid
    killAsyncProcess _ = return ()


executePipeline :: Handle -> Handle -> Pipeline -> Execution Process
executePipeline stdin stdout SimplePipeline { pipe_command=command } = executeCommand stdin stdout command

executePipeline stdin stdout Pipeline { pipe_head=command, pipe_tail=tail } = do
    -- createPipe: exception is error
    (newStdin, newStdout) <- liftIO $ createPipe
    myFinally (do executeCommand stdin newStdout command
                  executePipeline newStdin stdout tail)
              (liftIO $ hClose newStdout >> hClose newStdin)

executeList :: Handle -> Handle -> List -> Execution Process
executeList stdin stdout ListOne { lone_pipeline=pipeline } =
  executePipeline stdin stdout pipeline

executeList stdin stdout ListSeq { lseq_first=first, lseq_second=second } = do
  process <- executeList stdin stdout first
  lift $ getProcessCode process
  executeList stdin stdout second

executeList stdin stdout ListAnd { land_pipeline=pipeline, land_tail=list } = do
  process <- executePipeline stdin stdout pipeline
  code <- lift $ getProcessCode' process
  case code of
    ExitSuccess -> do
      executeList stdin stdout list
    _ -> return process

executeList stdin stdout ListOr { lor_pipeline=pipeline, lor_tail=list } = do
  process <- executePipeline stdin stdout pipeline
  code <- lift $ getProcessCode' process
  case code of
    ExitSuccess -> return process
    _ -> executeList stdin stdout list

executeList stdin stdout ListAsync { lasync_list=list } = do
    asyncProcesses <- lift $ gets es_asyncProcesses
    config <- lift $ lift ask
    state <- lift $ get
    -- forkIO: exception is error
    liftIO $ forkIO $ runAsync config state asyncProcesses
    return $ ProcessDummy ExitSuccess
  where
    updateAsyncProcessInfo :: AsyncProcessState -> AsyncProcessId -> AsyncProcessInfo -> AsyncProcessInfo
    updateAsyncProcessInfo s i (AsyncProcessInfo i' p Running) | i' == i = AsyncProcessInfo i' p s
    updateAsyncProcessInfo _ _ x = x

    runAsync :: Config -> ShellState -> TVar [AsyncProcessInfo] -> IO ()
    runAsync config state asyncProcesses = do
      threadId <- myThreadId

      newId <- liftIO $ atomically $ do
        ps <- readTVar asyncProcesses
        let newId = AsyncProcessId $ length ps
        writeTVar asyncProcesses $ (AsyncProcessInfo newId threadId Running) : ps
        return newId

      putStrLn $ show newId ++ " Init -> Running"

      result <- withFile "/dev/null" ReadMode $ \nullInput -> do
        runShell False config state $ execute $ do
          pid <- executeList nullInput System.IO.stdout list
          lift $ getProcessCode pid

      let text = case result of
                         Right code -> show code
                         Left error -> show error

      atomically $ do
        ps <- readTVar asyncProcesses
        writeTVar asyncProcesses $ map (updateAsyncProcessInfo (Done text) newId) ps

      putStrLn $ show newId ++ " Running -> " ++ show (Done text)
