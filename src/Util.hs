module Util where

import Control.Monad
import Control.Monad.Except
import System.Console.ANSI
import System.IO

whenM :: Monad m => m Bool -> m () -> m ()
whenM mb a = mb >>= \b -> when b a

hPutStrLnVerbose :: Handle -> String -> IO ()
hPutStrLnVerbose h s = setSGR [SetColor Foreground Vivid Yellow] >> hPutStr h s >> setSGR [Reset] >> hPutStrLn h ""

hPutStrLnError :: Handle -> String -> IO ()
hPutStrLnError h s = setSGR [SetColor Foreground Vivid Red] >> hPutStr h s >> setSGR [Reset] >> hPutStrLn h ""

myFinally :: Monad m => ExceptT e m a -> ExceptT e m b -> ExceptT e m a
myFinally m f = do
    result <- m `catchError` \e -> f >> throwError e
    f
    return result

constM :: Monad m => a -> b -> m a
constM a b = return a
