{-# LANGUAGE DeriveDataTypeable #-}

module Main where

import Funsh
import Shell
import System.Console.Haskeline
import Control.Monad.Trans.Class
import System.Console.CmdArgs
import qualified System.Directory (getCurrentDirectory)
import qualified System.Environment (getEnvironment)
import System.IO hiding (stdin, stdout, stderr)
import qualified System.IO (stdin, stdout, stderr)

data Funsh = Funsh {
  verbose :: Bool
} deriving (Show, Data, Typeable)

funsh = Funsh {
  verbose = def &= help "Print debug information"
}

main :: IO ()
main = do
    cwd <- System.Directory.getCurrentDirectory
    Funsh { verbose=verbose } <- cmdArgs funsh
    let config = Config {
      cfg_verbose = verbose
    }
    env <- System.Environment.getEnvironment
    state <- makeInitialShellState cwd env
    runShell True config state $ runInputT defaultSettings loop
  where 
    loop :: InputT Shell ()
    loop = do
      input <- getInputLine "$ "
      case input of
        Nothing -> return ()
        Just "exit" -> return ()
        Just input' -> do
          lift $ run System.IO.stdin System.IO.stdout False input'
          loop
