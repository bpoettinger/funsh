# funsh

A shell similar to Bash.
It does not strive for total compatibility and implements only a subset of Bash.

**Attention**: be careful when using this application.
Dangerous things can be done using a shell and I do not know if there are some edge cases that alter the effect relative to the actual/intended effect.
During development and testing no issues were observered and the behaviour was as expected/described in this file, but I do explicitly not guarantee that this application will not cause damage.
In doubt test this application in a virtual machine or container.

## Build, Install and Run

Attention: funsh was developed and tested on Linux (Arch Linux, Ubuntu, NixOS) only!

To build and install funsh execute from the root directory of the project:

```bash
stack install
```

Now you can execute `funsh-exe`.
The optional parameter `-v` enables printing the abstract syntax tree of every command executed within funsh to `/tmp/debug.svg`.

Note that `stack install` may print a notice if stack's install directory is not in the path and therefore the path of the executable will not be resolved automatically.
Note also that running funsh using `stack exec funsh-exe` the `PATH ` environment variables is not inherited from the forking process, i.e. many executables will not be found.
When Running `funsh-exe` after `stack install` this issue does not apply.

When starting funsh it expects the environment variables `HOME` and `PATH` being set in the calling shell.

## Project Setup

The project uses stack for project management.
The project layout is the default one of stack.
`README.md` is this file and acts as a manual for this project/application.
`package.yaml`, `Setup.hs` and `stack.yaml` are part of stack.
`LICENSE` contains this project's license.
The `test`-folder contains some software tests, which can be executed using `stack test`.
The `src`-folder contains the major part of this application as a software library.
The `app`-folder contains the source for the actual executable file which uses the library to realize the application.

## User Interface

The user interface shows the input request line
```text
$
```

Type your commands and the output of the commands are printed.
When the commands terminated the input request line appears again.
The user-interface can be left using the command `exit` or the key-combination `Ctrl+D`.
The key-combination `Ctrl+C` does also terminate the shell.
This behaviour differs from Bash which terminates the currently running command.

## Language

The language of funsh is similar to Bash but there are some differences.

Commands are the simplest component of the language.
There are a bunch of different commands.
At the moment we only consider simple commands, compound commands are elaborated later.

* **Simple Commands**: `command args...`.
  The first word `command` is resolved relative to the current working directory and `PATH`.
  E.g. in `ls -al /tmp`, `ls` is resolved to e.g. `/usr/bin/ls` and the arguments `-al` and `/tmp` are passed as arguments to the forked process running `/usr/bin/ls`.

  Commands can read from streams and write to streams.
  By default those streams are `stdin` and `stdout`, the streams representing the user-input from the terminal and the command-output to the terminal.
  But the streams can be redirected by the following syntax for the input case: `command args... < file`.
  This command replaces `stdin` by a file stream sourced by the stated file.
  Analogously the output case: `command args > file`.
  This command writes instead to `stdout` to `file`.
  The content of the destination file is always deleted.
  To append the content to the current content of the destination file, use `command args >> file`.
  The redirection operators can be combined arbitrarily (i.e. `commands args > file1 < file2 >> file3 < file4`).
  If multiple input/output redirections occur the last one will be applied, the other ones are ignored.
  Files must occur only once in all redirections active at the same time (also applies for pipelines, see later).

  Every command returns upon termination a return code.
  `0` indicates successful termination and all other codes indicate non-successful termination.
  Return codes are used by lists.

  The environment can be set for simple commands the following way: `var=value command args...`, i.e. command is provided with the environment variable `var`.
  The effects is limited to this single simple command.

Building upon commands (the simple ones as well as the compound ones, which are described later), pipelines are defined:

* **Pipeline**: Every command reads input from an ingoing stream and its output is written to an outgoing stream.
  Connecting the output and the input of two consecutive commands is called a pipeline.
  The syntax for pipelines is `<command> | <command> | ... | <command>`.
  E.g. `ls | wc -l` redirects the output of `ls` to the input of `wc`, which again writes its output to `stdout` (i.e. the stream visible in the terminal).
  If `ls` read input the input would have been read from `stdin`, i.e. the stream representing the inputs of the terminal.

  The return code of a pipeline is the return code of the last stage of the pipeline.

Pipelines in turn are organized in four different kinds of lists:

* **And-List**: Two pipelines combined by the AND-operator `<pipeline> && <pipeline>` have the following semantics:
  if the first pipeline does not terminate successfully, then the second one is not executed and the return code of the whole term equals the return code of the first pipeline.
  If the first pipeline terminates successfully, then the second one is executed and the return code of the whole term equals the return code of the second pipeline.
* **Or-List**: Two pipeline combined by the OR-operator `<pipeline> || <pipeline>`.
  If the first pipeline terminates successfully, then the second one is not executed and the return code of the whole term equals the return code of the first pipeline.
  If the first pipeline does not terminate successfilly, then the second one is executed and the return code of the whole term equals the return code of the second pipeline.
* **Sequential List**: The sequential list `<l-list>; <l-list>; ...; <l-list>` is made up by list elements of the previous two forms (called `l-list` here).
  Every list is executed one after another regardless of the return code of the element lists.
  The return code is the return code of the last element list.
* **Async-List**: Sequential list elements can be executed asynchronously by appending a `&` to the list element.
  Async lists are assumed to terminate successfully despite the actual success.
  Information on async lists can be requested using the `jobs` command.
  The `kill` command can terminate async lists.
  Async commands provide some logging to `stdout` on termination.

There are not only simple commands but also compound ones combining simpler commands to more complex ones:

* **Arithmetic Statements**: `((<stmt>, <stmt>, ..., <stmt>))`.
  Arithmetic statements similar to C can be executed, except for increment and decrement operators.
  E.g. `((x=3+6, y=x*6, x/=4, z=x&&y||y))`.
  Operators available are addition, subtraction, multiplication, division, modulo, negation, comparisons and logical and, or and not.
  Conditional operators `condition ? then : else` are available.
  Logical operators consider values of `0` as false and all other values as true.
  The return code is implied by the last statement: if the value is `!=0` then the return code is success (`0`), otherwise the return code is `1`.
* **Test Statements**: E.g. `[[ -e file1 && x==blub || !-f file2 ]]`.
  Test operators are `-e` (path exists), `-d` (direcory exists), `-f` (file exists), each one followed by a path.
  Those statements can be combined using logical operators and, or and not.
  Additionally string comparisons (with lexicographical ordering) are available: `==`, `<=`, `>=`, `<`, `>`, `!=`.
  If the term is true then the statement evaluates to `0`, otherwise to `1`.
* **for-Loop**: `for <var> in <word>...<word>; do <list>; done` executes the body list with `<var>` set to every of the `<word>`s once.
  The return code equals the code of the last body invocation.
  If there are no invocations, then the loop terminates successfully.
* **while-Loop**, **until-Loop**: `while <list>; do <list>; done` and `until <list>; do <list>; done`. Analogously to for-Loop.
* **for-Loop (C-style)**: there is a C-style for-loop `for ((init; condition; increment)); do <list>; done` expecting arithmetic statements for the three components `init`, `condition` and `increment`.
  Remember that increment and decrement operators are not available and must be written `x+=1` instead of `x++` or `++x`.
* **Conditionals**: `if <list>; then <list>; else <list>; fi`. As expected.
* **Variable Assignment**: `variable=value`. If `variable` is exported the environment value is also updated.

Additional to simple and compound commands there are builtin commands:

* `cd path`: Change directory. Sets the processes current working directory.
* `export var`: Export a variable to the environment of the process. Child processes inherit those valies.
* `export var=value`: Same as previous point but provide a value.
* `retain var`: inverse of export, i.e. remove variable from environment of process.
* `jobs` shows a list of all async lists and their state.
* `kill n` terminates job number `n`.

Variables are used in the default context the following ways:

* `${variable}` is substituted by the according value.
  Defaults to the empty string if the variable is not set.
* `$((arithmetic))` substitutes the result of the (last) arithmetic statement in the parentheses.
* `~` can occur as the first character of a word and is substituted with the `HOME` value.
* `$?` substitutes the return code of the last executed list.
* The glob operator `*` can occur anywhere inside words.
  If it occurs in a word the word is substituted by all filenames matching the pattern.
  The glob operator is applied last of all the substitutions.

In the default context the so-called control character `;&|\<>` must not occur in not escaped form.
To state them they must be escaped prefixing them with `\`, e.g. `\|`.
There is also the singly-quoted context `'blablabla'` which does not apply any substitutions, i.e. the above substitution patterns are being kept unchanged.
The doubly-quoted context `"dfhkjsdhfk"` applies substitutions but keeps the meaning of control characters.

## Non-Intuitive Limitations and Differences of funsh Compared to Bash

* There are no brace operators and command substitution.
* The effects of substitution differ slightly and not all substitutions of bash are available.
* Variable substitutions must not be abreviated `$VAR` but must be `${VAR}`.
* The asynchronous execution of lists has a different syntax. Instead of `&` only, use `&;`.

## License

BDS 3-Clause. See the file `LICENSE`.
